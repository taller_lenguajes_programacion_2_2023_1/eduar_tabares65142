import { Component } from '@angular/core';
import { Router } from '@angular/router';  
import { AuthService } from 'src/app/services/login/auth.service' 
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms'; 
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  username: string='';
  password: string='';
  
  user = {
    username: this.username,
    password: this.password
  }

  constructor(private authService: AuthService,private router: Router,private http: HttpClient) {}

    onSubmit() {
      // Realizar una solicitud GET al backend para obtener la lista de usuarios
      this.http.get('http://localhost:9001/finapp/users').subscribe(
        (value: Object) => {
          // La solicitud GET fue exitosa y se obtuvo la lista de usuarios del backend
          const users = Array.from(value as any[]);
          const foundUser = users.find(user => user.userName === this.username && user.password === this.password);
          if (foundUser) {
            this.router.navigate(['/ExpensesList']);
          } else {
            alert('Credenciales incorrectas');
          }
        },
        (error) => {
          console.error(error);
          alert('Error al obtener los datos de usuarios');
        }
      );
    }      
}
