import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor() { }

  save(key: string, value: any): void {
    // obtiene los valores previos guardados en sessionStorage, ??'' es para que si key viene null le envíe una cadena vacía
    const previousValues = JSON.parse(sessionStorage.getItem(key)??'[]') ;
    
    // agrega el nuevo valor a la lista de valores previos
    previousValues.push(value);

    // guarda la lista actualizada de valores en sessionStorage
    sessionStorage.setItem(key, JSON.stringify(previousValues));
  }
}
