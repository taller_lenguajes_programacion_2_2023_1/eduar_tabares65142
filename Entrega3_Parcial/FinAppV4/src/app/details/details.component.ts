import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import listaDeGastos from 'src/assets/gastos.json';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent {

  gasto: any;
  constructor(private route: ActivatedRoute) { }

    getCategoriaImagen(categoria: string) {
      
    switch (categoria) {
      case 'Alimentación':
        return '../assets/alimentacion.png';
      case 'Transporte':
        return '../assets/transporte.png';
      case 'Casa':
        return '../assets/casa.png';
      case 'Entretenimiento':
        return '../assets/entretenimiento.png';
      case 'Salud':
        return '../assets/salud.png';
      default:
        return null  
    }
  }

  ngOnInit(): void {
    const item = this.route.snapshot.paramMap.get('item');
    this.gasto = listaDeGastos.find(p => p.item === item);
  }

}
