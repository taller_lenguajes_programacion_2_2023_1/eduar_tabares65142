package com.finapp.Finapp.repository;

import com.finapp.Finapp.DAO.UsersDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<UsersDAO, Long> {

    //UsersDAO findByid(String id_users);

    UsersDAO findByUserName(String username);

    //UsersDAO findByName(String name);

    //UsersDAO findByPassword(String password);
}
