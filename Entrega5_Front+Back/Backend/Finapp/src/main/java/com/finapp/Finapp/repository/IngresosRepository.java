package com.finapp.Finapp.repository;

import com.finapp.Finapp.DAO.GastosDAO;
import com.finapp.Finapp.DAO.IngresosDAO;
import com.finapp.Finapp.domain.Ingresos.Ingresos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngresosRepository extends JpaRepository<IngresosDAO, Long> {

    //IngresosDAO findById(String id_gastos);
    IngresosDAO findByCategory(String categoria);
    //IngresosDAO findByDetail(String detail);
    //IngresosDAO findByValue(Double value_monetary);
}
