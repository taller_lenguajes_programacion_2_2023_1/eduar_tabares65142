package com.finapp.Finapp;

import com.finapp.Finapp.domain.Gastos.*;

public class FinappApplicationTest {
    public static void main(String[] args) {
        System.out.println("Hola mundo");

        try {
            GastosId gastosId = new GastosId(45l);
            GastosDetail gastosDetail = new GastosDetail("Transporte");
            GastosValue gastosValue = new GastosValue(1800000d);
            GastosCategory gastosCategory= new GastosCategory("Transporte");

            Gastos gastos = new Gastos(gastosId, gastosDetail, gastosCategory, gastosValue);

            System.out.println(gastos);
        }catch (NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
