package com.finapp.Finapp.domain.Users;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UsersFirstName {

    private final String value;

    public UsersFirstName(String value) {
        this.value = value;
    }
}
