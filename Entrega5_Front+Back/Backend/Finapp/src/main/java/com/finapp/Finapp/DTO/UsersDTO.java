package com.finapp.Finapp.DTO;

import com.finapp.Finapp.domain.Users.*;
import lombok.Data;

@Data
public class UsersDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String phone;
    private String userName;
    private String password;

    public Users toDomain(){
        UsersId usersId = new UsersId(this.id);
        UsersFirstName usersFirstName = new UsersFirstName(this.firstName);
        UsersLastName usersLastName = new UsersLastName(this.lastName);
        UsersPhone usersPhone = new UsersPhone(this.phone);
        UsersUserName usersUserName = new UsersUserName(this.userName);
        UsersPassword usersPassword = new UsersPassword(this.password);

        Users users  = new Users(usersId, usersFirstName, usersLastName, usersPhone, usersUserName,usersPassword);
        return users;
    }

    public static UsersDTO fromDomain(Users users){
        UsersDTO usersDTO = new UsersDTO();
        usersDTO.setId(users.getId().getValue());
        usersDTO.setFirstName(users.getFirstName().getValue());
        usersDTO.setLastName(users.getLastName().getValue());
        usersDTO.setPhone(users.getPhone().getValue());
        usersDTO.setUserName(users.getUserName().getValue());
        usersDTO.setPassword(users.getPassword().getValue());
        //Long id = gastos.getId() == null ? 0 : gastos.getId().getValue();
        //gastosDAO.setId_gastos(id);
        return usersDTO;
    }
}
