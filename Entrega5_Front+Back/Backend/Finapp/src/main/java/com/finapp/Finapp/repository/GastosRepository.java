package com.finapp.Finapp.repository;

import com.finapp.Finapp.DAO.GastosDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GastosRepository extends JpaRepository<GastosDAO, Long> {

    //GastosDAO findById(String id_gastos);
    GastosDAO findByCategory(String categoria);
    GastosDAO findByDetail(String detail);
   // GastosDAO findByValue(Double value);
}
