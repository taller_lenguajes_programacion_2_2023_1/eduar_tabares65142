package com.finapp.Finapp.domain.Ingresos;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class IngresosCategory {
    private final String value;

    public IngresosCategory(String value) {
        this.value = value;
    }
}
