package com.finapp.Finapp.domain.Ingresos;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class IngresosValue {

    private final Double value;


    public IngresosValue(Double value) {
        this.value = value;
    }
}
