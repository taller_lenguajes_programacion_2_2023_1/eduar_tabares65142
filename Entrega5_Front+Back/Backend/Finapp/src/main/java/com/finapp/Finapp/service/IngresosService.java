package com.finapp.Finapp.service;

import com.finapp.Finapp.DAO.GastosDAO;
import com.finapp.Finapp.DAO.IngresosDAO;
import com.finapp.Finapp.DTO.GastosDTO;
import com.finapp.Finapp.DTO.IngresosDTO;
import com.finapp.Finapp.domain.Gastos.Gastos;
import com.finapp.Finapp.domain.Ingresos.Ingresos;
import com.finapp.Finapp.domain.Ingresos.IngresosCategory;
import com.finapp.Finapp.domain.Ingresos.IngresosDetail;
import com.finapp.Finapp.domain.Ingresos.IngresosValue;
import com.finapp.Finapp.repository.IngresosRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class IngresosService {

    private final IngresosRepository ingresosRepository;

    public IngresosService(IngresosRepository ingresosRepository) {
        this.ingresosRepository = ingresosRepository;
    }

    public IngresosDTO save(IngresosDTO ingresosDTO){
        IngresosDAO ingresosDb = this.ingresosRepository.findByCategory(ingresosDTO.getCategory());

        if (ingresosDb != null) throw new IllegalArgumentException("Name of product is not unique");

        IngresosDetail ingresosDetail = new IngresosDetail(ingresosDTO.getDetail());
        IngresosCategory ingresosCategory = new IngresosCategory(ingresosDTO.getCategory());
        IngresosValue ingresosValue = new IngresosValue(ingresosDTO.getValor());

        Ingresos ingresos = new Ingresos(null, ingresosDetail, ingresosCategory, ingresosValue);

        IngresosDAO ingregosForSave = IngresosDAO.fromDomain(ingresosDTO.toDomain());
        IngresosDAO ingresosSaved = this.ingresosRepository.save(ingregosForSave);
        Ingresos ingresosSavedWithDomain = ingresosSaved.toDomain();

        return IngresosDTO.fromDomain(ingresosSavedWithDomain);
    }

    public List<IngresosDTO> getAllIngresos() {
        List<IngresosDAO> ingresosDAOS = this.ingresosRepository.findAll();
        List<Ingresos> ingresos = ingresosDAOS.stream().map(ingresosDAO -> ingresosDAO.toDomain()).collect(Collectors.toList());
        List<IngresosDTO> ingresosDTOS = ingresos.stream().map(ingreso -> IngresosDTO.fromDomain(ingreso)).collect(Collectors.toList());
        return ingresosDTOS;
    }
}
