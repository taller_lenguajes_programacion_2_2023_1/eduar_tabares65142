package com.finapp.Finapp.DTO;

import com.finapp.Finapp.DAO.IngresosDAO;
import com.finapp.Finapp.domain.Ingresos.*;
import lombok.Data;

@Data
public class IngresosDTO {

    private Long id;
    private String detail;
    private String category;
    private Double valor;

    public Ingresos toDomain(){
        IngresosId ingresosId = new IngresosId(id);
        IngresosCategory ingresosCategory = new IngresosCategory(category);
        IngresosDetail ingresosDetail = new IngresosDetail(detail);
        IngresosValue ingresosValue = new IngresosValue(valor);

        Ingresos ingresos = new Ingresos(ingresosId,ingresosDetail,ingresosCategory,ingresosValue);
        return ingresos;
    }

    public static IngresosDTO fromDomain(Ingresos ingresos){
        IngresosDTO ingresosDTO = new IngresosDTO();
        ingresosDTO.setValor(ingresos.getValue().getValue());
        ingresosDTO.setCategory(ingresos.getCategory().getValue());
        ingresosDTO.setDetail(ingresos.getDetail().getValue());
        ingresosDTO.setId(ingresos.getId().getValue());
        //Long id = gastos.getId() == null ? 0 : gastos.getId().getValue();
        //gastosDAO.setId_gastos(id);
        return ingresosDTO;
    }
}
