package com.finapp.Finapp.domain.Users;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UsersPassword {

    private final String value;

    public UsersPassword(String value) {
        this.value = value;
    }
}
