export interface User {
    nombre: string;
    apellido: string;
    celular: number;
    usuario: string;
    clave: string;
}
