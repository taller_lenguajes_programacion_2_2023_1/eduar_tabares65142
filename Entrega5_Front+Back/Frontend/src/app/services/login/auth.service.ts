import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) { }

  login(username: string, password: string): Observable<boolean> {
    return this.http.get<any>('http://localhost:9001/finapp/users')
    .pipe(
      map((users: any[]) =>{
        const authenticatedUser = users.find(user => user.username === username && user.password === password);
        return authenticatedUser != null;
      })
    );
     /* .pipe(
        map(users => {
          const user = users.find((u: { username: string; password: string; firstname: string; lastname: string; phone: string; token:'abc123' }) => u.username === username && u.password === password);
          if (user) {
            localStorage.setItem('currentUser', JSON.stringify(user));
            return true;
          } else {
            return false;
          }
        })
      );*/
  }

  
  
  loginStorage(userName: string, password: string ): boolean {
    const usersData = sessionStorage.getItem('userData');
    if (!usersData) {
      console.log("No hay datos en sessionstorage");
      return false;
    }
    const users = JSON.parse(usersData);
    for (const u of users) {
      console.log(u.userName);
      console.log(u.password);
      if (u.userName === userName && u.password === password) {
        return true;
      }
    }
    console.log("No coinciden los datos");
    return false;
  }

  isLoggedIn() {
    const currentUser = JSON.parse(sessionStorage.getItem('currentUser')??'');
    if(currentUser  && currentUser.token){
      const user = JSON.parse(currentUser);
      return user && user.token;
    }else
      return false;
  }

  logout() {
    this.router.navigate(['/']);
  }
}
