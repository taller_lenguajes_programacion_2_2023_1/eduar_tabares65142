import { Component } from '@angular/core';
import { Router } from '@angular/router';
import listaDeGastos from 'src/assets/gastos.json';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-expenses-list',
  templateUrl: './expenses-list.component.html',
  styleUrls: ['./expenses-list.component.css']
})
export class ExpensesListComponent {

  gastos: any = listaDeGastos;
  gastosBack: any[] = [];
  nuevoGastoDetalle: string = '';
  nuevoGastoCategoria: string = '';
  nuevoGastoValor: number = 0; 

  constructor(private router:Router,private http: HttpClient){}

  ngOnInit(){
    this.http.get<any[]>('http://localhost:9001/finapp/gastos').subscribe(
      (value: any[]) => {
        this.gastosBack = value;
      },
      (error) => {
        console.error(error);
        alert('Error al obtener los datos de gastos');
      }
    );
  }

  agregarGasto() {

    const nuevoGasto = {
      detail: this.nuevoGastoDetalle,
      category: this.nuevoGastoCategoria,
      valor: this.nuevoGastoValor
    };

    /*this.http.post('http://localhost:9001/finapp/gastos', nuevoGasto).subscribe(
      (response) => {
       this.http.get<any[]>('http://localhost:9001/finapp/gastos').subscribe(
          (value) => {
            // La solicitud GET fue exitosa y se obtuvo la lista actualizada de gastos del backend
            this.gastos = value;
          }
          (error) => {
            console.error(error);
            alert('Error al obtener los datos de gastos');
          }
        );
      },
      (error) => {
        console.error(error);
        alert('Error al guardar el gasto');
      }
    );*/

    this.http.post('http://localhost:9001/finapp/gastos', nuevoGasto).subscribe(
      (response) => {
        // La solicitud POST fue exitosa y el gasto se guardó en el backend
        this.gastos.push(nuevoGasto);

        // Reiniciar los valores de los campos de nuevo gasto
        this.nuevoGastoDetalle = '';
        this.nuevoGastoCategoria = '';
        this.nuevoGastoValor = 0;
        this.ngOnInit();
      },
      (error) => {
        console.error(error);
        alert('Error al guardar el gasto');
      }
    );

  }
}
