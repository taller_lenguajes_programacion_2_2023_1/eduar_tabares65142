(function(window, undefined) {
  var dictionary = {
    "958d79e6-402b-4de4-983e-2e49a5a31635": "Ingresos",
    "da5fb122-b5d7-4837-8bd8-8814ec98b8fe": "Gastos",
    "0e50b75d-b568-427d-b8ad-5d4e98fcfdee": "Login",
    "9f60b5b9-810a-44cc-a23e-e7ba9f863900": "Registro",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Vista General",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "ef07b413-721c-418e-81b1-33a7ed533245": "960 grid - 12 columns",
    "e73b655d-d3ec-4dcc-a55c-6e0293422bde": "960 grid - 16 columns",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);