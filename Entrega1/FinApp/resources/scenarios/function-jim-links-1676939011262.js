(function(window, undefined) {

  var jimLinks = {
    "958d79e6-402b-4de4-983e-2e49a5a31635" : {
      "Paragraph_28" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Paragraph_29" : [
        "da5fb122-b5d7-4837-8bd8-8814ec98b8fe"
      ],
      "Hotspot_2" : [
        "da5fb122-b5d7-4837-8bd8-8814ec98b8fe"
      ],
      "Hotspot_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    },
    "da5fb122-b5d7-4837-8bd8-8814ec98b8fe" : {
      "Paragraph_28" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Paragraph_33" : [
        "958d79e6-402b-4de4-983e-2e49a5a31635"
      ],
      "Hotspot_2" : [
        "da5fb122-b5d7-4837-8bd8-8814ec98b8fe"
      ],
      "Hotspot_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Rectangle_2" : [
        "958d79e6-402b-4de4-983e-2e49a5a31635"
      ]
    },
    "0e50b75d-b568-427d-b8ad-5d4e98fcfdee" : {
      "Paragraph_1" : [
        "9f60b5b9-810a-44cc-a23e-e7ba9f863900"
      ],
      "Button_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    },
    "9f60b5b9-810a-44cc-a23e-e7ba9f863900" : {
      "Paragraph_1" : [
        "0e50b75d-b568-427d-b8ad-5d4e98fcfdee"
      ],
      "Button_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "Paragraph_25" : [
        "958d79e6-402b-4de4-983e-2e49a5a31635"
      ],
      "Paragraph_29" : [
        "da5fb122-b5d7-4837-8bd8-8814ec98b8fe"
      ],
      "Paragraph_33" : [
        "958d79e6-402b-4de4-983e-2e49a5a31635",
        "958d79e6-402b-4de4-983e-2e49a5a31635"
      ],
      "Image_24" : [
        "958d79e6-402b-4de4-983e-2e49a5a31635"
      ],
      "Paragraph_35" : [
        "da5fb122-b5d7-4837-8bd8-8814ec98b8fe"
      ],
      "Hotspot_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Hotspot_2" : [
        "da5fb122-b5d7-4837-8bd8-8814ec98b8fe"
      ],
      "Rectangle_22" : [
        "958d79e6-402b-4de4-983e-2e49a5a31635"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);