var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1400" deviceHeight="900">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677124341279.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-da5fb122-b5d7-4837-8bd8-8814ec98b8fe" class="screen growth-both devWeb canvas PORTRAIT firer commentable non-processed" alignment="center" name="Gastos" width="1400" height="900">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/da5fb122-b5d7-4837-8bd8-8814ec98b8fe-1677124341279.css" />\
      <div class="freeLayout">\
      <div id="s-Dynamic_Panel_1" class="dynamicpanel firer ie-background commentable pin hpin-center non-processed-pin non-processed" customid="Dynamic_Panel_1" datasizewidth="1401.0px" datasizeheight="900.0px" dataX="0.0" dataY="60.0" >\
        <div id="s-Panel_1" class="panel default firer ie-background commentable non-processed" customid="Panel_1"  datasizewidth="1401.0px" datasizeheight="900.0px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Rectangle_6" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_6"   datasizewidth="1400.0px" datasizeheight="900.0px" datasizewidthpx="1400.0" datasizeheightpx="900.0" dataX="1.0" dataY="0.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_6_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_5" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_5"   datasizewidth="361.0px" datasizeheight="53.0px" dataX="281.0" dataY="43.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_5_0">Gastos</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_34" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_34"   datasizewidth="151.0px" datasizeheight="24.0px" dataX="45.0" dataY="34.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_34_0">FINAPP</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Rectangle_13" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_13"   datasizewidth="2.0px" datasizeheight="16.0px" datasizewidthpx="2.0" datasizeheightpx="16.0" dataX="32.0" dataY="36.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_13_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_37" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_37"   datasizewidth="237.0px" datasizeheight="29.0px" dataX="282.0" dataY="450.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_37_0">Categorias</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Chart_1" class="group firer ie-background commentable non-processed" customid="Chart_1" datasizewidth="1090.0px" datasizeheight="285.0px" >\
                  <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Group_2" datasizewidth="1088.0px" datasizeheight="285.0px" >\
                    <div id="s-Table_2" class="table firer commentable non-processed" customid="Table_2"  datasizewidth="433.4px" datasizeheight="260.0px" dataX="907.8" dataY="581.0" originalwidth="432.35045871559635px" originalheight="258.9999999999999px" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <table summary="">\
                            <tbody>\
                              <tr>\
                                <td id="s-Text_cell_361" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_361"     datasizewidth="87.3px" datasizeheight="48.0px" dataX="0.0" dataY="0.0" originalwidth="86.30004239849171px" originalheight="46.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_361_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_364" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_364"     datasizewidth="87.7px" datasizeheight="48.0px" dataX="203.0" dataY="0.0" originalwidth="86.72516576006063px" originalheight="46.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_364_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_367" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_367"     datasizewidth="87.3px" datasizeheight="48.0px" dataX="407.0" dataY="0.0" originalwidth="86.30004239849171px" originalheight="46.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_367_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_370" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_370"     datasizewidth="87.3px" datasizeheight="48.0px" dataX="610.0" dataY="0.0" originalwidth="86.30004239849171px" originalheight="46.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_370_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_373" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_373"     datasizewidth="87.7px" datasizeheight="48.0px" dataX="813.0" dataY="0.0" originalwidth="86.72516576006063px" originalheight="46.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_373_0"></span></div></div></div></div></div></div>  </td>\
                              </tr>\
                              <tr>\
                                <td id="s-Text_cell_362" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_362"     datasizewidth="87.3px" datasizeheight="45.0px" dataX="0.0" dataY="47.0" originalwidth="86.30004239849171px" originalheight="43.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_362_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_365" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_365"     datasizewidth="87.7px" datasizeheight="45.0px" dataX="203.0" dataY="47.0" originalwidth="86.72516576006063px" originalheight="43.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_365_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_368" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_368"     datasizewidth="87.3px" datasizeheight="45.0px" dataX="407.0" dataY="47.0" originalwidth="86.30004239849171px" originalheight="43.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_368_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_371" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_371"     datasizewidth="87.3px" datasizeheight="45.0px" dataX="610.0" dataY="47.0" originalwidth="86.30004239849171px" originalheight="43.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_371_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_374" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_374"     datasizewidth="87.7px" datasizeheight="45.0px" dataX="813.0" dataY="47.0" originalwidth="86.72516576006063px" originalheight="43.99999999999998px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_374_0"></span></div></div></div></div></div></div>  </td>\
                              </tr>\
                              <tr>\
                                <td id="s-Text_cell_363" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_363"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="0.0" dataY="91.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_363_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_366" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_366"     datasizewidth="87.7px" datasizeheight="43.0px" dataX="203.0" dataY="91.0" originalwidth="86.72516576006063px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_366_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_369" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_369"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="407.0" dataY="91.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_369_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_372" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_372"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="610.0" dataY="91.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_372_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_375" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_375"     datasizewidth="87.7px" datasizeheight="43.0px" dataX="813.0" dataY="91.0" originalwidth="86.72516576006063px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_375_0"></span></div></div></div></div></div></div>  </td>\
                              </tr>\
                              <tr>\
                                <td id="s-Text_cell_391" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_391"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="0.0" dataY="133.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_391_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_392" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_392"     datasizewidth="87.7px" datasizeheight="43.0px" dataX="203.0" dataY="133.0" originalwidth="86.72516576006063px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_392_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_393" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_393"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="407.0" dataY="133.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_393_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_394" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_394"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="610.0" dataY="133.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_394_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_395" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_395"     datasizewidth="87.7px" datasizeheight="43.0px" dataX="813.0" dataY="133.0" originalwidth="86.72516576006063px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_395_0"></span></div></div></div></div></div></div>  </td>\
                              </tr>\
                              <tr>\
                                <td id="s-Text_cell_401" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_401"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="0.0" dataY="175.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_401_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_402" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_402"     datasizewidth="87.7px" datasizeheight="43.0px" dataX="203.0" dataY="175.0" originalwidth="86.72516576006063px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_402_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_403" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_403"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="407.0" dataY="175.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_403_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_404" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_404"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="610.0" dataY="175.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_404_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_405" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_405"     datasizewidth="87.7px" datasizeheight="43.0px" dataX="813.0" dataY="175.0" originalwidth="86.72516576006063px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_405_0"></span></div></div></div></div></div></div>  </td>\
                              </tr>\
                              <tr>\
                                <td id="s-Text_cell_411" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_411"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="0.0" dataY="217.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_411_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_412" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_412"     datasizewidth="87.7px" datasizeheight="43.0px" dataX="203.0" dataY="217.0" originalwidth="86.72516576006063px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_412_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_413" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_413"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="407.0" dataY="217.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_413_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_414" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_414"     datasizewidth="87.3px" datasizeheight="43.0px" dataX="610.0" dataY="217.0" originalwidth="86.30004239849171px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_414_0"></span></div></div></div></div></div></div>  </td>\
                                <td id="s-Text_cell_415" class="textcell manualfit firer ie-background non-processed" customid="Text_cell_415"     datasizewidth="87.7px" datasizeheight="43.0px" dataX="813.0" dataY="217.0" originalwidth="86.72516576006063px" originalheight="41.999999999999986px" ><div class="cellContainerChild"><div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div><div class="borderLayer"><div class="paddingLayer"><div class="clipping"><div class="content"><div class="valign"><span id="rtr-s-Text_cell_415_0"></span></div></div></div></div></div></div>  </td>\
                              </tr>\
                            </tbody>\
                          </table>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_10" class="richtext autofit firer ie-background commentable non-processed" customid="Text_10"   datasizewidth="45.8px" datasizeheight="16.0px" dataX="942.3" dataY="850.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_10_0">Week 1</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_11" class="richtext autofit firer ie-background commentable non-processed" customid="Text_11"   datasizewidth="48.6px" datasizeheight="16.0px" dataX="1027.8" dataY="850.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_11_0">Week 2</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_12" class="richtext autofit firer ie-background commentable non-processed" customid="Text_12"   datasizewidth="48.7px" datasizeheight="16.0px" dataX="1112.6" dataY="850.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_12_0">Week 3</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_13" class="richtext autofit firer ie-background commentable non-processed" customid="Text_13"   datasizewidth="48.8px" datasizeheight="16.0px" dataX="1198.5" dataY="850.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_13_0">Week 4</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_19" class="richtext autofit firer ie-background commentable non-processed" customid="Text_19"   datasizewidth="49.1px" datasizeheight="16.0px" dataX="1290.9" dataY="849.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_19_0">Week 5</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_6" class="richtext autofit firer ie-background commentable non-processed" customid="Text_6"   datasizewidth="44.3px" datasizeheight="16.0px" dataX="878.0" dataY="591.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_6_0">$1</span><span id="rtr-s-Text_6_1">,00</span><span id="rtr-s-Text_6_2">0</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_7" class="richtext autofit firer ie-background commentable non-processed" customid="Text_7"   datasizewidth="33.5px" datasizeheight="16.0px" dataX="882.3" dataY="640.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_7_0">$7</span><span id="rtr-s-Text_7_1">50</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_8" class="richtext autofit firer ie-background commentable non-processed" customid="Text_8"   datasizewidth="35.7px" datasizeheight="16.0px" dataX="881.8" dataY="688.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_8_0">$6</span><span id="rtr-s-Text_8_1">00</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_23" class="richtext autofit firer ie-background commentable non-processed" customid="Text_23"   datasizewidth="33.8px" datasizeheight="16.0px" dataX="882.3" dataY="736.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_23_0">$4</span><span id="rtr-s-Text_23_1">50</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_24" class="richtext autofit firer ie-background commentable non-processed" customid="Text_24"   datasizewidth="35.0px" datasizeheight="16.0px" dataX="881.8" dataY="784.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_24_0">$3</span><span id="rtr-s-Text_24_1">00</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Text_25" class="richtext autofit firer ie-background commentable non-processed" customid="Text_25"   datasizewidth="30.8px" datasizeheight="16.0px" dataX="883.5" dataY="832.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Text_25_0">$1</span><span id="rtr-s-Text_25_1">50</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Line_4" class="path firer ie-background commentable non-processed" customid="Line_4"   datasizewidth="12.2px" datasizeheight="3.0px" dataX="900.5" dataY="598.5"  >\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg xmlns="http://www.w3.org/2000/svg" width="11.216514587402344" height="2.0" viewBox="900.4871559633027 598.4999999999999 11.216514587402344 2.0" preserveAspectRatio="none">\
                        	  <g>\
                        	    <defs>\
                        	      <path id="s-Line_4-da5fb" d="M900.9871559633027 599.4999999999999 L911.2036697247706 599.4999999999999 "></path>\
                        	    </defs>\
                        	    <g style="mix-blend-mode:normal">\
                        	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Line_4-da5fb" fill="none" stroke-width="1.0" stroke="#D9D9D9" stroke-linecap="butt"></use>\
                        	    </g>\
                        	  </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Line_5" class="path firer ie-background commentable non-processed" customid="Line_5"   datasizewidth="12.2px" datasizeheight="3.0px" dataX="900.5" dataY="647.5"  >\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg xmlns="http://www.w3.org/2000/svg" width="11.216514587402344" height="2.0" viewBox="900.4871559633027 647.4999999999998 11.216514587402344 2.0" preserveAspectRatio="none">\
                        	  <g>\
                        	    <defs>\
                        	      <path id="s-Line_5-da5fb" d="M900.9871559633027 648.4999999999998 L911.2036697247706 648.4999999999998 "></path>\
                        	    </defs>\
                        	    <g style="mix-blend-mode:normal">\
                        	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Line_5-da5fb" fill="none" stroke-width="1.0" stroke="#D9D9D9" stroke-linecap="butt"></use>\
                        	    </g>\
                        	  </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Line_6" class="path firer ie-background commentable non-processed" customid="Line_6"   datasizewidth="12.2px" datasizeheight="3.0px" dataX="900.5" dataY="695.5"  >\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg xmlns="http://www.w3.org/2000/svg" width="11.216514587402344" height="2.0" viewBox="900.4871559633027 695.4999999999998 11.216514587402344 2.0" preserveAspectRatio="none">\
                        	  <g>\
                        	    <defs>\
                        	      <path id="s-Line_6-da5fb" d="M900.9871559633027 696.4999999999998 L911.2036697247706 696.4999999999998 "></path>\
                        	    </defs>\
                        	    <g style="mix-blend-mode:normal">\
                        	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Line_6-da5fb" fill="none" stroke-width="1.0" stroke="#D9D9D9" stroke-linecap="butt"></use>\
                        	    </g>\
                        	  </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Line_8" class="path firer ie-background commentable non-processed" customid="Line_8"   datasizewidth="12.2px" datasizeheight="3.0px" dataX="900.5" dataY="743.5"  >\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg xmlns="http://www.w3.org/2000/svg" width="11.216514587402344" height="2.0" viewBox="900.4871559633027 743.4999999999998 11.216514587402344 2.0" preserveAspectRatio="none">\
                        	  <g>\
                        	    <defs>\
                        	      <path id="s-Line_8-da5fb" d="M900.9871559633027 744.4999999999998 L911.2036697247706 744.4999999999998 "></path>\
                        	    </defs>\
                        	    <g style="mix-blend-mode:normal">\
                        	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Line_8-da5fb" fill="none" stroke-width="1.0" stroke="#D9D9D9" stroke-linecap="butt"></use>\
                        	    </g>\
                        	  </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Path_1" class="path firer ie-background commentable non-processed" customid="Line_9"   datasizewidth="12.2px" datasizeheight="3.0px" dataX="900.5" dataY="791.5"  >\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg xmlns="http://www.w3.org/2000/svg" width="11.216514587402344" height="2.0" viewBox="900.4871559633027 791.4999999999998 11.216514587402344 2.0" preserveAspectRatio="none">\
                        	  <g>\
                        	    <defs>\
                        	      <path id="s-Path_1-da5fb" d="M900.9871559633027 792.4999999999998 L911.2036697247706 792.4999999999998 "></path>\
                        	    </defs>\
                        	    <g style="mix-blend-mode:normal">\
                        	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_1-da5fb" fill="none" stroke-width="1.0" stroke="#D9D9D9" stroke-linecap="butt"></use>\
                        	    </g>\
                        	  </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Path_2" class="path firer ie-background commentable non-processed" customid="Line_10"   datasizewidth="12.2px" datasizeheight="3.0px" dataX="900.5" dataY="839.5"  >\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg xmlns="http://www.w3.org/2000/svg" width="11.216514587402344" height="2.0" viewBox="900.4871559633027 839.4999999999995 11.216514587402344 2.0" preserveAspectRatio="none">\
                        	  <g>\
                        	    <defs>\
                        	      <path id="s-Path_2-da5fb" d="M900.9871559633027 840.4999999999995 L911.2036697247706 840.4999999999995 "></path>\
                        	    </defs>\
                        	    <g style="mix-blend-mode:normal">\
                        	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_2-da5fb" fill="none" stroke-width="1.0" stroke="#D9D9D9" stroke-linecap="butt"></use>\
                        	    </g>\
                        	  </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
\
\
                  <div id="s-Image_26" class="image firer ie-background commentable non-processed" customid="Image_26"   datasizewidth="432.1px" datasizeheight="219.0px" dataX="909.1" dataY="621.0"   alt="image" systemName="./images/01e627c6-556a-450b-abbe-a27c87a7efee.svg" overlay="#5C7FF7">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="429px" version="1.1" viewBox="0 0 921 429" width="921px">\
                      	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
                      	    <title>Path</title>\
                      	    <desc>Created with Sketch.</desc>\
                      	    <g fill="none" fill-rule="evenodd" id="s-Image_26-Page-1" stroke="none" stroke-width="1">\
                      	        <polygon fill="#67B7DC" id="s-Image_26-Path" points="0 365.6 5 375.4 10 355.9 15 351 20 341.2 25 365.6 30 321.7 35 316.8 40 331.4 45 346 50 350.9 55 341.1 60 326.5 64 302.1 69 311.9 74 287.5 79 272.9 84 341.2 89 311.9 94 321.7 99 341.2 104 336.3 109 360.7 114 355.8 119 370.4 123 389.9 128 385 133 389.9 138 394.8 143 404.6 148 375.3 153 365.5 158 341.1 163 331.3 168 287.4 173 267.9 178 224 183 258.1 187 277.6 192 199.6 197 175.2 202 204.5 207 228.9 212 219.1 217 233.7 222 238.6 227 263 232 287.4 237 263 242 248.4 246 224 251 189.9 256 204.5 261 199.6 266 160.6 271 141.1 276 146 281 150.9 286 131.4 291 141.2 296 102.2 301 112 306 131.5 310 107.1 315 92.5 320 82.7 325 102.2 330 121.7 335 204.6 340 272.9 345 326.6 350 341.2 355 326.6 360 292.5 365 297.4 369 253.5 374 268.1 379 277.9 384 282.8 389 263.3 394 243.8 399 248.7 404 214.6 409 190.2 414 170.7 419 151.2 424 136.6 429 131.7 433 92.7 438 102.5 443 78.1 448 53.7 453 63.5 458 87.9 463 78.1 468 87.9 473 78.1 478 73.2 483 102.5 488 97.6 492 112.2 497 82.9 502 63.4 507 68.3 512 82.9 517 58.5 522 53.6 527 34.1 532 24.3 537 38.9 542 34 547 4.7 552 29.1 556 9.6 561 38.9 566 4.8 571 24.3 576 14.5 581 19.4 586 29.2 591 73.1 596 82.9 601 63.4 606 43.9 611 87.8 615 73.2 620 131.7 625 126.8 630 107.3 635 112.2 640 73.2 645 43.9 650 48.8 655 48.8 660 48.8 665 68.3 670 73.2 675 63.4 679 87.8 684 53.7 689 102.5 694 126.9 699 117.1 704 131.7 709 141.5 714 170.8 719 165.9 724 156.1 729 141.5 734 146.4 738 161 743 175.6 748 165.8 753 185.3 758 185.3 763 180.4 768 175.5 773 146.2 778 136.4 783 102.3 788 116.9 793 107.1 798 136.4 802 121.8 807 131.6 812 136.5 817 112.1 822 63.3 827 53.5 832 48.6 837 87.6 842 87.6 847 73 852 82.8 857 68.2 861 48.7 866 14.6 871 29.2 876 24.3 881 -0.1 886 14.5 891 14.5 896 38.9 901 4.8 906 19.4 911 24.3 916 19.4 921 34 921 429 0 429" style="fill:#5C7FF7 !important;" />\
                      	    </g>\
                      	</svg>\
\
                      </div>\
                    </div>\
                  </div>\
\
\
                  <div id="s-Image_1" class="image firer ie-background commentable non-processed" customid="Image_1"   datasizewidth="433.4px" datasizeheight="209.0px" dataX="908.6" dataY="619.0"   alt="image" systemName="./images/01017a86-4578-462b-9af9-da7a7911db31.svg" overlay="#5C7FF7">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="412px" version="1.1" viewBox="0 0 923 412" width="923px">\
                      	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
                      	    <title>Path</title>\
                      	    <desc>Created with Sketch.</desc>\
                      	    <g fill="none" fill-rule="evenodd" id="s-Image_1-Page-1" stroke="none" stroke-width="1">\
                      	        <polyline id="s-Image_1-Path" points="1 369.6 6 379.3 11 359.8 16 355 21 345.2 26 369.6 31 325.7 36 320.8 41 335.4 46 350.1 51 355 56 345.2 61 330.6 65 306.2 70 315.9 75 291.5 80 276.9 85 345.2 90 315.9 95 325.7 100 345.2 105 340.3 110 364.7 115 359.8 120 374.5 124 394 129 389.1 134 394 139 398.9 144 408.6 149 379.3 154 369.6 159 345.2 164 335.4 169 291.5 174 272 179 228.1 184 262.3 188 281.8 193 203.7 198 179.4 203 208.6 208 233 213 223.3 218 237.9 223 242.8 228 267.2 233 291.5 238 267.2 243 252.5 247 228.1 252 194 257 208.6 262 203.7 267 164.7 272 145.2 277 150.1 282 155 287 135.5 292 145.2 297 106.2 302 115.9 307 135.5 311 111.1 316 96.4 321 86.7 326 106.2 331 125.7 336 208.6 341 276.9 346 330.6 351 345.2 356 330.6 361 296.4 366 301.3 370 257.4 375 272 380 281.8 385 286.7 390 267.2 395 247.6 400 252.5 405 218.4 410 194 415 174.5 420 155 425 140.3 430 135.5 434 96.4 439 106.2 444 81.8 449 57.4 454 67.2 459 91.6 464 81.8 469 91.6 474 81.8 479 76.9 484 106.2 489 101.3 493 115.9 498 86.7 503 67.2 508 72 513 86.7 518 62.3 523 57.4 528 37.9 533 28.1 538 42.8 543 37.9 548 8.6 553 33 557 13.5 562 42.8 567 8.6 572 28.1 577 18.4 582 23.3 587 33 592 76.9 597 86.7 602 67.2 607 47.7 612 91.6 616 76.9 621 135.5 626 130.6 631 111.1 636 115.9 641 76.9 646 47.7 651 52.5 656 52.5 661 52.5 666 72 671 76.9 676 67.2 680 91.6 685 57.4 690 106.2 695 130.6 700 120.8 705 135.5 710 145.2 715 174.5 720 169.6 725 159.8 730 145.2 735 150.1 739 164.7 744 179.4 749 169.6 754 189.1 759 189.1 764 184.2 769 179.4 774 150.1 779 140.3 784 106.2 789 120.8 794 111.1 799 140.3 803 125.7 808 135.5 813 140.3 818 115.9 823 67.2 828 57.4 833 52.5 838 91.6 843 91.6 848 76.9 853 86.7 858 72 862 52.5 867 18.4 872 33 877 28.1 882 3.8 887 18.4 892 18.4 897 42.8 902 8.6 907 23.3 912 28.1 917 23.3 922 37.9" stroke="#EE7B0E" stroke-width="2" style="stroke:#5C7FF7 !important;" />\
                      	    </g>\
                      	</svg>\
\
                      </div>\
                    </div>\
                  </div>\
\
                </div>\
\
                <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_1"   datasizewidth="20.0px" datasizeheight="20.0px" datasizewidthpx="20.0" datasizeheightpx="20.0" dataX="640.5" dataY="521.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_1_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_27" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_27"   datasizewidth="159.0px" datasizeheight="29.0px" dataX="674.5" dataY="522.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_27_0">Cultura y ocio</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_35" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_35"   datasizewidth="112.0px" datasizeheight="38.0px" dataX="674.5" dataY="565.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_35_0">Cuentas y pagos</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Rectangle_3" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_3"   datasizewidth="20.0px" datasizeheight="20.0px" datasizewidthpx="20.0" datasizeheightpx="20.0" dataX="640.5" dataY="565.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_3_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_36" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_36"   datasizewidth="102.0px" datasizeheight="29.0px" dataX="674.5" dataY="615.3" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_36_0">Ahorros</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Rectangle_4" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_4"   datasizewidth="20.0px" datasizeheight="20.0px" datasizewidthpx="20.0" datasizeheightpx="20.0" dataX="640.5" dataY="615.3" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_4_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_38" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_38"   datasizewidth="102.0px" datasizeheight="29.0px" dataX="674.5" dataY="664.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_38_0">Donaciones</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Rectangle_5" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_5"   datasizewidth="20.0px" datasizeheight="20.0px" datasizewidthpx="20.0" datasizeheightpx="20.0" dataX="640.5" dataY="664.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_5_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_40" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_40"   datasizewidth="112.0px" datasizeheight="29.0px" dataX="674.5" dataY="709.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_40_0">Ropa</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Rectangle_7" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_7"   datasizewidth="20.0px" datasizeheight="20.0px" datasizewidthpx="20.0" datasizeheightpx="20.0" dataX="640.5" dataY="709.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_7_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_41" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_41"   datasizewidth="112.0px" datasizeheight="29.0px" dataX="674.5" dataY="759.5" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_41_0">Transporte</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Rectangle_8" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_8"   datasizewidth="20.0px" datasizeheight="20.0px" datasizewidthpx="20.0" datasizeheightpx="20.0" dataX="640.5" dataY="758.5" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_8_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_42" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_42"   datasizewidth="122.0px" datasizeheight="38.0px" dataX="674.5" dataY="804.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_42_0">Bienestar y Salud</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Rectangle_9" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_9"   datasizewidth="20.0px" datasizeheight="20.0px" datasizewidthpx="20.0" datasizeheightpx="20.0" dataX="640.5" dataY="803.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_9_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Dynamic_Panel_2" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic_Panel_2" datasizewidth="426.0px" datasizeheight="302.0px" dataX="914.0" dataY="183.0" >\
                  <div id="s-Panel_2" class="panel default firer ie-background commentable non-processed" customid="Panel_2"  datasizewidth="426.0px" datasizeheight="302.0px" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                    	<div class="layoutWrapper scrollable">\
                    	  <div class="paddingLayer">\
                          <div class="freeLayout">\
                          <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Group_4" datasizewidth="383.0px" datasizeheight="590.0px" >\
                            <div id="s-Paragraph_65" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_65"   datasizewidth="221.0px" datasizeheight="42.0px" dataX="29.0" dataY="17.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_65_0">American airlines Reembolso</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_66" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_66"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="42.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_66_0">RECIBIDO</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_67" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_67"   datasizewidth="206.0px" datasizeheight="29.0px" dataX="29.0" dataY="77.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_67_0">De Carlos Suarez.</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_68" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_68"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="102.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_68_0">RECIBIDO</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_70" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_70"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="137.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_70_0">Starbucks</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_71" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_71"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="165.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_71_0">ENVIADO</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_72" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_72"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="221.0" dataY="17.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_72_0">$ +240.60</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_73" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_73"   datasizewidth="98.0px" datasizeheight="27.0px" dataX="288.0" dataY="77.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_73_0">$ +197.75</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_74" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_74"   datasizewidth="98.0px" datasizeheight="27.0px" dataX="288.0" dataY="137.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_74_0">$ -24.12</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
\
                            <div id="s-Image_34" class="image lockV firer ie-background commentable non-processed" customid="Image_34"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="5.0" dataY="19.0" aspectRatio="1.1333333"  rotationdeg="180.0" alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#5C7FF7">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#5C7FF7 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
\
                            <div id="s-Image_35" class="image lockV firer ie-background commentable non-processed" customid="Image_35"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="7.0" dataY="79.0" aspectRatio="1.1333333"  rotationdeg="180.0" alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#5C7FF7">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#5C7FF7 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
\
                            <div id="s-Image_36" class="image lockV firer ie-background commentable non-processed" customid="Image_36"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="5.0" dataY="139.0" aspectRatio="1.1333333"   alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#8995B2">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#8995B2 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
                            <div id="s-Paragraph_69" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_69"   datasizewidth="206.0px" datasizeheight="29.0px" dataX="29.0" dataY="192.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_69_0">De Ana Martinez</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_75" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_75"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="217.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_75_0">RECIBIDO</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_76" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_76"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="252.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_76_0">Starbucks</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_77" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_77"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="278.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_77_0">ENVIADO</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_78" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_78"   datasizewidth="98.0px" datasizeheight="27.0px" dataX="288.0" dataY="192.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_78_0">$ +37.80</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_79" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_79"   datasizewidth="98.0px" datasizeheight="27.0px" dataX="288.0" dataY="252.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_79_0">$ -24.12</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
\
                            <div id="s-Image_37" class="image lockV firer ie-background commentable non-processed" customid="Image_37"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="7.0" dataY="196.0" aspectRatio="1.1333333"  rotationdeg="180.0" alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#5C7FF7">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#5C7FF7 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
\
                            <div id="s-Image_38" class="image lockV firer ie-background commentable non-processed" customid="Image_38"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="5.0" dataY="254.0" aspectRatio="1.1333333"   alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#8995B2">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#8995B2 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
                            <div id="s-Paragraph_81" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_81"   datasizewidth="190.0px" datasizeheight="25.0px" dataX="29.0" dataY="317.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_81_0">American airlines Refund</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_82" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_82"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="342.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_82_0">RECEIVED</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_83" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_83"   datasizewidth="206.0px" datasizeheight="29.0px" dataX="29.0" dataY="377.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_83_0">Personal transfer - Ben F.J.</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_84" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_84"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="402.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_84_0">RECEIVED</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_85" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_85"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="438.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_85_0">Asos</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_86" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_86"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="465.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_86_0">SENT</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_87" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_87"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="221.0" dataY="317.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_87_0">$ +125.80</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_88" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_88"   datasizewidth="98.0px" datasizeheight="27.0px" dataX="288.0" dataY="377.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_88_0">$ +197.75</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_89" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_89"   datasizewidth="98.0px" datasizeheight="27.0px" dataX="288.0" dataY="438.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_89_0">$ -87.21</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
\
                            <div id="s-Image_39" class="image lockV firer ie-background commentable non-processed" customid="Image_39"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="5.0" dataY="319.0" aspectRatio="1.1333333"  rotationdeg="180.0" alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#5C7FF7">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#5C7FF7 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
\
                            <div id="s-Image_40" class="image lockV firer ie-background commentable non-processed" customid="Image_40"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="7.0" dataY="379.0" aspectRatio="1.1333333"  rotationdeg="180.0" alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#5C7FF7">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#5C7FF7 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
\
                            <div id="s-Image_41" class="image lockV firer ie-background commentable non-processed" customid="Image_41"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="5.0" dataY="439.0" aspectRatio="1.1333333"   alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#8995B2">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#8995B2 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
                            <div id="s-Paragraph_90" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_90"   datasizewidth="206.0px" datasizeheight="29.0px" dataX="29.0" dataY="492.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_90_0">Personal transfer - linda</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_91" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_91"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="517.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_91_0">RECEIVED</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_92" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_92"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="552.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_92_0">Apple Store</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_93" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_93"   datasizewidth="165.0px" datasizeheight="29.0px" dataX="29.0" dataY="578.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_93_0">SENT</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_94" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_94"   datasizewidth="98.0px" datasizeheight="27.0px" dataX="288.0" dataY="492.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_94_0">$ +58.56</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
                            <div id="s-Paragraph_95" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_95"   datasizewidth="98.0px" datasizeheight="27.0px" dataX="288.0" dataY="552.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_95_0">$ -380.99</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>\
\
                            <div id="s-Image_42" class="image lockV firer ie-background commentable non-processed" customid="Image_42"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="7.0" dataY="496.0" aspectRatio="1.1333333"  rotationdeg="180.0" alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#5C7FF7">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#5C7FF7 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
\
                            <div id="s-Image_43" class="image lockV firer ie-background commentable non-processed" customid="Image_43"   datasizewidth="15.0px" datasizeheight="17.0px" dataX="5.0" dataY="554.0" aspectRatio="1.1333333"   alt="image" systemName="./images/965b54f1-26ed-404a-a557-adaec4494b2f.svg" overlay="#8995B2">\
                              <div class="borderLayer">\
                              	<div class="imageViewport">\
                                	<?xml version="1.0" encoding="UTF-8"?>\
                                	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-arrow-left fa-w-14" data-icon="arrow-left" data-prefix="fas" role="img" viewBox="0 0 448 512"><path d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" fill="currentColor" style="fill:#8995B2 !important;" /></svg>\
\
                                </div>\
                              </div>\
                            </div>\
\
                          </div>\
\
                          </div>\
\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Path_3" class="path firer ie-background commentable non-processed" customid="Line_7"   datasizewidth="3.0px" datasizeheight="389.0px" dataX="869.5" dataY="138.5"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="388.0" viewBox="869.5 138.5 2.0 388.0" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_3-da5fb" d="M870.5 139.0 L870.5 526.0 "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_3-da5fb" fill="none" stroke-width="1.0" stroke="#D9D9D9" stroke-linecap="butt"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_39" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_39"   datasizewidth="188.0px" datasizeheight="29.0px" dataX="282.0" dataY="96.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_39_0">Mes Actual</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Group_1" datasizewidth="116.0px" datasizeheight="22.0px" >\
                  <div id="s-Paragraph_80" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_80"   datasizewidth="108.0px" datasizeheight="20.0px" dataX="1076.0" dataY="500.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_80_0">View all details</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
\
                  <div id="s-Image_5" class="image lockV firer ie-background commentable non-processed" customid="Image_5"   datasizewidth="7.0px" datasizeheight="13.0px" dataX="1185.0" dataY="503.0" aspectRatio="1.8571428"   alt="image" systemName="./images/ffe5fc53-26cd-458e-8390-faeef1ab14c9.svg" overlay="#8995B2">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="27px" version="1.1" viewBox="0 0 15 27" width="15px">\
                      	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
                      	    <title>Arrow Right</title>\
                      	    <desc>Created with Sketch.</desc>\
                      	    <defs />\
                      	    <g fill="none" fill-rule="evenodd" id="s-Image_5-Page-1" stroke="none" stroke-width="1">\
                      	        <g fill="#DDDDDD" id="s-Image_5-Components" transform="translate(-463.000000, -1276.000000)">\
                      	            <g id="s-Image_5-Arrow-Right" transform="translate(463.000000, 1276.000000)">\
                      	                <polyline points="1.157 0 0 1.144 12.325 13.62 0 25.691 1.325 27 15 13.691 1.157 0" style="fill:#8995B2 !important;" />\
                      	            </g>\
                      	        </g>\
                      	    </g>\
                      	</svg>\
\
                      </div>\
                    </div>\
                  </div>\
\
                  <div id="s-Path_4" class="path firer ie-background commentable non-processed" customid="Line_1"   datasizewidth="101.0px" datasizeheight="3.0px" dataX="1077.5" dataY="520.5"  >\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg xmlns="http://www.w3.org/2000/svg" width="100.0" height="2.0" viewBox="1077.5 520.5 100.0 2.0" preserveAspectRatio="none">\
                      	  <g>\
                      	    <defs>\
                      	      <path id="s-Path_4-da5fb" d="M1078.0 521.5 L1177.0 521.5 "></path>\
                      	    </defs>\
                      	    <g style="mix-blend-mode:normal">\
                      	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_4-da5fb" fill="none" stroke-width="1.0" stroke="#8995B2" stroke-linecap="butt" opacity="0.5"></use>\
                      	    </g>\
                      	  </g>\
                      	</svg>\
\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Paragraph_43" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_43"   datasizewidth="335.0px" datasizeheight="29.0px" dataX="281.0" dataY="544.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_43_0">Evolution of general expenses</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-culture_expenses" class="group firer ie-background commentable hidden non-processed" customid="culture_expenses" datasizewidth="131.0px" datasizeheight="72.0px" >\
                  <div id="s-Paragraph_25" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_25"   datasizewidth="102.0px" datasizeheight="29.0px" dataX="388.0" dataY="315.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_25_0">Culture</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_26" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_26"   datasizewidth="131.0px" datasizeheight="51.0px" dataX="373.0" dataY="336.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_26_0">$ 340.25</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
\
                <div id="s-restaurants_expenses" class="group firer ie-background commentable hidden non-processed" customid="restaurants_expenses" datasizewidth="131.0px" datasizeheight="72.0px" >\
                  <div id="s-Paragraph_44" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_44"   datasizewidth="102.0px" datasizeheight="29.0px" dataX="389.0" dataY="315.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_44_0">Restaurants</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_45" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_45"   datasizewidth="131.0px" datasizeheight="51.0px" dataX="374.0" dataY="336.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_45_0">$ 125.40</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
\
                <div id="s-food_expenses" class="group firer ie-background commentable hidden non-processed" customid="food_expenses" datasizewidth="131.0px" datasizeheight="72.0px" >\
                  <div id="s-Paragraph_46" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_46"   datasizewidth="102.0px" datasizeheight="29.0px" dataX="388.0" dataY="315.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_46_0">Food</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_47" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_47"   datasizewidth="131.0px" datasizeheight="51.0px" dataX="373.0" dataY="336.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_47_0">$ 417.22</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
\
                <div id="s-donations_expenses" class="group firer ie-background commentable hidden non-processed" customid="donations_expenses" datasizewidth="131.0px" datasizeheight="72.0px" >\
                  <div id="s-Paragraph_48" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_48"   datasizewidth="102.0px" datasizeheight="29.0px" dataX="388.0" dataY="315.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_48_0">Donations</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_49" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_49"   datasizewidth="131.0px" datasizeheight="51.0px" dataX="373.0" dataY="336.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_49_0">$ 60.70</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
\
                <div id="s-clothes_expenses" class="group firer ie-background commentable hidden non-processed" customid="clothes_expenses" datasizewidth="131.0px" datasizeheight="72.0px" >\
                  <div id="s-Paragraph_50" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_50"   datasizewidth="102.0px" datasizeheight="29.0px" dataX="389.0" dataY="315.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_50_0">Clothes</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_51" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_51"   datasizewidth="131.0px" datasizeheight="51.0px" dataX="374.0" dataY="336.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_51_0">$ 97.90</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
\
                <div id="s-transport_expenses" class="group firer ie-background commentable hidden non-processed" customid="transport_expenses" datasizewidth="131.0px" datasizeheight="72.0px" >\
                  <div id="s-Paragraph_52" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_52"   datasizewidth="102.0px" datasizeheight="29.0px" dataX="389.0" dataY="315.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_52_0">Transport</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_53" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_53"   datasizewidth="131.0px" datasizeheight="51.0px" dataX="374.0" dataY="336.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_53_0">$ 214.87</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
\
                <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="Group 3" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Donut" class="group firer ie-background commentable non-processed" customid="Donut" datasizewidth="310.0px" datasizeheight="308.0px" >\
                    <div id="s-Image_2" class="image firer click ie-background commentable non-processed" customid="Image_2"   datasizewidth="111.0px" datasizeheight="111.0px" dataX="277.0" dataY="678.0"   alt="image" systemName="./images/2c3caff8-c6f4-4235-8262-cae4669a8171.svg" overlay="#D9B4C1">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="189px" version="1.1" viewBox="0 0 187 189" width="187px">\
                        	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
                        	    <title>XMLID_7_</title>\
                        	    <desc>Created with Sketch.</desc>\
                        	    <g fill="none" fill-rule="evenodd" id="s-Image_2-Page-1" stroke="none" stroke-width="1">\
                        	        <path d="M186.4,71.9 L104,188.1 C50.6,150.2 13.6,93.4 0.6,29.3 L140.2,0.9 C146,29.6 162.5,55 186.4,71.9 Z" fill="#448E4D" id="s-Image_2-XMLID_7_" style="fill:#D9B4C1 !important;" />\
                        	    </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Image_3" class="image firer click ie-background commentable non-processed" customid="Image_3"   datasizewidth="159.0px" datasizeheight="96.0px" dataX="339.0" dataY="724.0"   alt="image" systemName="./images/11c7b7b2-39d0-4d2f-9eb0-a08a44831976.svg" overlay="#524AA5">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="165px" version="1.1" viewBox="0 0 269 165" width="269px">\
                        	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
                        	    <title>XMLID_2_</title>\
                        	    <desc>Created with Sketch.</desc>\
                        	    <g fill="none" fill-rule="evenodd" id="s-Image_3-Page-1" stroke="none" stroke-width="1">\
                        	        <path d="M202.3,9.1 L268.2,135.4 C182.5,180.1 78.9,173.1 5.68434189e-14,117.1 L82.4,0.9 C117.6,25.9 163.9,29.1 202.3,9.1 Z" fill="#304175" id="s-Image_3-XMLID_2_" style="fill:#524AA5 !important;" />\
                        	    </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Image_14" class="image firer click ie-background commentable non-processed" customid="Image_14"   datasizewidth="80.0px" datasizeheight="88.0px" dataX="460.0" dataY="712.0"   alt="image" systemName="./images/92d6238b-ff64-4673-87db-ffa807825355.svg" overlay="#B1A7F1">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="151px" version="1.1" viewBox="0 0 135 151" width="135px">\
                        	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
                        	    <title>XMLID_3_</title>\
                        	    <desc>Created with Sketch.</desc>\
                        	    <g fill="none" fill-rule="evenodd" id="s-Image_14-Page-1" stroke="none" stroke-width="1">\
                        	        <path d="M31,0.8 L134.9,98.3 C115.1,119.4 91.8,137.1 66.2,150.5 L0.3,24.2 C11.7,18.1 22.1,10.2 31,0.8 Z" fill="#CD82AD" id="s-Image_14-XMLID_3_" style="fill:#B1A7F1 !important;" />\
                        	    </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Image_21" class="image firer click ie-background commentable non-processed" customid="Image_21"   datasizewidth="76.0px" datasizeheight="65.0px" dataX="478.0" dataY="702.0"   alt="image" systemName="./images/92f750ef-99f2-4227-a559-bb3551c2793b.svg" overlay="#6AADD5">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="112px" version="1.1" viewBox="0 0 128 112" width="128px">\
                        	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
                        	    <title>XMLID_4_</title>\
                        	    <desc>Created with Sketch.</desc>\
                        	    <g fill="none" fill-rule="evenodd" id="s-Image_21-Page-1" stroke="none" stroke-width="1">\
                        	        <path d="M10.4,0.9 L127.2,82.4 C120.1,92.6 112.3,102.2 103.8,111.2 L0,13.8 C3.8,9.7 7.3,5.4 10.4,0.9 Z" fill="#CC4848" id="s-Image_21-XMLID_4_" style="fill:#6AADD5 !important;" />\
                        	    </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Image_22" class="image firer click ie-background commentable non-processed" customid="Image_22"   datasizewidth="120.0px" datasizeheight="219.0px" dataX="463.0" dataY="531.0"   alt="image" systemName="./images/f3d13214-6835-4558-94b1-229542401496.svg" overlay="#B2C3FE">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="374px" version="1.1" viewBox="0 0 203 374" width="203px">\
                        	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
                        	    <title>XMLID_5_</title>\
                        	    <desc>Created with Sketch.</desc>\
                        	    <g fill="none" fill-rule="evenodd" id="s-Image_22-Page-1" stroke="none" stroke-width="1">\
                        	        <path d="M0.7,125.2 L69.6,0.5 C194.1,69.3 239.3,226 170.4,350.5 C166.1,358.3 161.3,365.9 156.2,373.3 L39.4,291.8 C75.8,239.7 63,167.9 10.8,131.5 C7.6,129.3 4.2,127.2 0.7,125.2 Z" fill="#85B861" id="s-Image_22-XMLID_5_" style="fill:#B2C3FE !important;" />\
                        	    </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Image_23" class="image firer click ie-background commentable non-processed" customid="Image_23"   datasizewidth="76.0px" datasizeheight="91.0px" dataX="428.0" dataY="513.0"   alt="image" systemName="./images/1bdbb753-9675-495a-a001-a976e1cb32a1.svg" overlay="#8ED2E1">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="157px" version="1.1" viewBox="0 0 121 157" width="121px">\
                        	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
                        	    <title>XMLID_6_</title>\
                        	    <desc>Created with Sketch.</desc>\
                        	    <g fill="none" fill-rule="evenodd" id="s-Image_23-Page-1" stroke="none" stroke-width="1">\
                        	        <path d="M0,142.9 L0,0.4 C42.2,0.4 83.7,10.8 121,30.6 L54.1,156.4 C37.4,147.5 18.9,142.9 0,142.9 Z" fill="#69B8DC" id="s-Image_23-XMLID_6_" style="fill:#8ED2E1 !important;" />\
                        	    </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Image_25" class="image firer click ie-background commentable non-processed" customid="Image_25"   datasizewidth="153.0px" datasizeheight="182.0px" dataX="273.0" dataY="512.0"   alt="image" systemName="./images/79e67710-9486-4bb2-9b8e-ba589ff75a3e.svg" overlay="#5C7FF7">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="310px" version="1.1" viewBox="0 0 258 310" width="258px">\
                        	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
                        	    <title>XMLID_8_</title>\
                        	    <desc>Created with Sketch.</desc>\
                        	    <g fill="none" fill-rule="evenodd" id="s-Image_25-Page-1" stroke="none" stroke-width="1">\
                        	        <path d="M145.2,280.9 L5.6,309.3 C-22.7,169.8 67.3,33.9 206.7,5.6 C223.6,2.2 240.7,0.4 258,0.4 L258,142.9 C194.4,142.9 142.9,194.4 142.9,258 C142.9,265.7 143.6,273.4 145.2,280.9 Z" fill="#B7B83E" id="s-Image_25-XMLID_8_" style="fill:#5C7FF7 !important;" />\
                        	    </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
\
                  </div>\
\
                  <div id="shapewrapper-s-Ellipse_2" customid="Ellipse_2" class="shapewrapper shapewrapper-s-Ellipse_2 non-processed"   datasizewidth="200.0px" datasizeheight="200.0px" datasizewidthpx="200.0" datasizeheightpx="200.0" dataX="330.0" dataY="566.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_2" class="svgContainer" style="width:100%; height:100%;">\
                          <g>\
                              <g clip-path="url(#clip-s-Ellipse_2)">\
                                      <ellipse id="s-Ellipse_2" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse_2" cx="100.0" cy="100.0" rx="100.0" ry="100.0">\
                                      </ellipse>\
                              </g>\
                          </g>\
                          <defs>\
                              <clipPath id="clip-s-Ellipse_2" class="clipPath">\
                                      <ellipse cx="100.0" cy="100.0" rx="100.0" ry="100.0">\
                                      </ellipse>\
                              </clipPath>\
                          </defs>\
                      </svg>\
                      <div class="paddingLayer">\
                          <div id="shapert-s-Ellipse_2" class="content firer" >\
                              <div class="valign">\
                                  <span id="rtr-s-Ellipse_2_0"></span>\
                              </div>\
                          </div>\
                      </div>\
                  </div>\
\
                  <div id="s-health_expenses" class="group firer ie-background commentable non-processed" customid="health_expenses" datasizewidth="131.0px" datasizeheight="72.0px" >\
                    <div id="s-Paragraph_54" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_54"   datasizewidth="102.0px" datasizeheight="29.0px" dataX="378.0" dataY="636.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Paragraph_54_0">Ahorros</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    <div id="s-Paragraph_55" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_55"   datasizewidth="131.0px" datasizeheight="51.0px" dataX="361.0" dataY="657.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Paragraph_55_0">$ 184.21</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
\
                </div>\
\
                <div id="s-Paragraph_63" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_63"   datasizewidth="361.0px" datasizeheight="37.0px" dataX="914.0" dataY="144.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_63_0">Detalles</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_28" class="richtext manualfit firer click ie-background commentable non-processed" customid="Paragraph_28"   datasizewidth="126.0px" datasizeheight="29.0px" dataX="73.0" dataY="103.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_28_0">Vista General</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_29" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_29"   datasizewidth="126.0px" datasizeheight="29.0px" dataX="73.0" dataY="171.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_29_0">Gastos</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Image_16" class="image lockV firer ie-background commentable non-processed" customid="Image_16"   datasizewidth="22.0px" datasizeheight="22.0px" dataX="38.0" dataY="101.0" aspectRatio="1.0"   alt="image" systemName="./images/347554ed-2d69-4a76-be2d-3cbdfb496249.svg" overlay="#8995B2">\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" height="512pt" viewBox="0 0 512 512" width="512pt"><path d="m197.332031 170.667969h-160c-20.585937 0-37.332031-16.746094-37.332031-37.335938v-96c0-20.585937 16.746094-37.332031 37.332031-37.332031h160c20.589844 0 37.335938 16.746094 37.335938 37.332031v96c0 20.589844-16.746094 37.335938-37.335938 37.335938zm-160-138.667969c-2.941406 0-5.332031 2.390625-5.332031 5.332031v96c0 2.945313 2.390625 5.335938 5.332031 5.335938h160c2.945313 0 5.335938-2.390625 5.335938-5.335938v-96c0-2.941406-2.390625-5.332031-5.335938-5.332031zm0 0" fill="#8995B2" jimofill=" " /><path d="m197.332031 512h-160c-20.585937 0-37.332031-16.746094-37.332031-37.332031v-224c0-20.589844 16.746094-37.335938 37.332031-37.335938h160c20.589844 0 37.335938 16.746094 37.335938 37.335938v224c0 20.585937-16.746094 37.332031-37.335938 37.332031zm-160-266.667969c-2.941406 0-5.332031 2.390625-5.332031 5.335938v224c0 2.941406 2.390625 5.332031 5.332031 5.332031h160c2.945313 0 5.335938-2.390625 5.335938-5.332031v-224c0-2.945313-2.390625-5.335938-5.335938-5.335938zm0 0" fill="#8995B2" jimofill=" " /><path d="m474.667969 512h-160c-20.589844 0-37.335938-16.746094-37.335938-37.332031v-96c0-20.589844 16.746094-37.335938 37.335938-37.335938h160c20.585937 0 37.332031 16.746094 37.332031 37.335938v96c0 20.585937-16.746094 37.332031-37.332031 37.332031zm-160-138.667969c-2.945313 0-5.335938 2.390625-5.335938 5.335938v96c0 2.941406 2.390625 5.332031 5.335938 5.332031h160c2.941406 0 5.332031-2.390625 5.332031-5.332031v-96c0-2.945313-2.390625-5.335938-5.332031-5.335938zm0 0" fill="#8995B2" jimofill=" " /><path d="m474.667969 298.667969h-160c-20.589844 0-37.335938-16.746094-37.335938-37.335938v-224c0-20.585937 16.746094-37.332031 37.335938-37.332031h160c20.585937 0 37.332031 16.746094 37.332031 37.332031v224c0 20.589844-16.746094 37.335938-37.332031 37.335938zm-160-266.667969c-2.945313 0-5.335938 2.390625-5.335938 5.332031v224c0 2.945313 2.390625 5.335938 5.335938 5.335938h160c2.941406 0 5.332031-2.390625 5.332031-5.335938v-224c0-2.941406-2.390625-5.332031-5.332031-5.332031zm0 0" fill="#8995B2" jimofill=" " /></svg>\
\
                    </div>\
                  </div>\
                </div>\
\
\
                <div id="s-Image_17" class="image lockV firer ie-background commentable non-processed" customid="Image_17"   datasizewidth="23.0px" datasizeheight="23.0px" dataX="37.0" dataY="169.0" aspectRatio="1.0"   alt="image" systemName="./images/91fbfdec-b1be-47ca-97ef-47487644fbb5.svg" overlay="#5C7FF7">\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 507.246 507.246" height="512" id="s-Image_17-Layer_1" viewBox="0 0 507.246 507.246" width="512"><path d="m457.262 89.821c-2.734-35.285-32.298-63.165-68.271-63.165h-320.491c-37.771 0-68.5 30.729-68.5 68.5v316.934c0 37.771 30.729 68.5 68.5 68.5h370.247c37.771 0 68.5-30.729 68.5-68.5v-256.333c-.001-31.354-21.184-57.836-49.985-65.936zm-388.762-31.165h320.492c17.414 0 32.008 12.261 35.629 28.602h-356.121c-13.411 0-25.924 3.889-36.5 10.577v-2.679c0-20.126 16.374-36.5 36.5-36.5zm370.246 389.934h-370.246c-20.126 0-36.5-16.374-36.5-36.5v-256.333c0-20.126 16.374-36.5 36.5-36.5h370.247c20.126 0 36.5 16.374 36.5 36.5v55.838h-102.026c-40.43 0-73.322 32.893-73.322 73.323s32.893 73.323 73.322 73.323h102.025v53.849c0 20.126-16.374 36.5-36.5 36.5zm36.5-122.349h-102.025c-22.785 0-41.322-18.537-41.322-41.323s18.537-41.323 41.322-41.323h102.025z" fill="#5C7FF7" jimofill=" " /><circle cx="379.16" cy="286.132" r="16.658" fill="#5C7FF7" jimofill=" " /></svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Rectangle_12" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_12"   datasizewidth="223.0px" datasizeheight="900.0px" datasizewidthpx="223.0" datasizeheightpx="900.0" dataX="0.0" dataY="0.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_12_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_33" class="richtext manualfit firer click ie-background commentable non-processed" customid="Paragraph_33"   datasizewidth="126.0px" datasizeheight="29.0px" dataX="73.0" dataY="239.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_33_0">Ingresos</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Image_24" class="image lockV firer ie-background commentable non-processed" customid="Image_24"   datasizewidth="18.0px" datasizeheight="27.0px" dataX="40.0" dataY="233.0" aspectRatio="1.5"   alt="image" systemName="./images/cad2b44c-e268-4387-b29d-f04000877d15.svg" overlay="#8995B2">\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<!-- Generator: Adobe Illustrator 22.0.0, SVG Export Plug-In  --><svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" xmlns:xlink="http://www.w3.org/1999/xlink" height="519px" style="enable-background:new 0 0 391 519;" version="1.1" viewBox="0 0 391 519" width="391px" x="0px" xml:space="preserve" y="0px">\
                    	<defs>\
                    	</defs>\
                    	<g>\
                    		<path d="M116.5,290h162.7c7.8,0,14.2-6.4,14.2-14.2s-6.4-14.2-14.2-14.2H116.5c-7.8,0-14.2,6.4-14.2,14.2S108.7,290,116.5,290z" fill="#8995B2" jimofill=" " />\
                    		<path d="M116.5,222h162.7c7.8,0,14.2-6.4,14.2-14.2s-6.4-14.2-14.2-14.2H116.5c-7.8,0-14.2,6.4-14.2,14.2S108.7,222,116.5,222z" fill="#8995B2" jimofill=" " />\
                    		<path d="M116.5,358h162.7c7.8,0,14.2-6.4,14.2-14.2s-6.4-14.2-14.2-14.2H116.5c-7.8,0-14.2,6.4-14.2,14.2S108.7,358,116.5,358z" fill="#8995B2" jimofill=" " />\
                    		<path d="M116.5,426h162.7c7.8,0,14.2-6.4,14.2-14.2s-6.4-14.2-14.2-14.2H116.5c-7.8,0-14.2,6.4-14.2,14.2S108.7,426,116.5,426z" fill="#8995B2" jimofill=" " />\
                    		<path d="M334.2,64h-28.5v-7.2c0-7.8-6.4-14.2-14.2-14.2h-40.9C244.2,17.8,221.4,0,195.5,0c-25.9,0-48.7,17.8-55.1,42.7H99.5   c-7.8,0-14.2,6.4-14.2,14.2V64H56.8C25.5,64,0,89.5,0,120.8v341.3C0,493.5,25.5,519,56.8,519h277.3c31.3,0,56.8-25.5,56.8-56.8   V120.8C391,89.5,365.5,64,334.2,64z M195.5,28.3c15.7,0,28.5,12.8,28.5,28.5c0,7.8,6.4,14.2,14.2,14.2h39.2v28.5   c0,4-3.2,7.2-7.2,7.2H120.8c-4,0-7.2-3.2-7.2-7.2V71h39.2c7.8,0,14.2-6.4,14.2-14.2C167,41.1,179.8,28.3,195.5,28.3z M120.8,135   h149.3c19.6,0,35.5-15.9,35.5-35.5v-7.2h28.5c15.7,0,28.5,12.8,28.5,28.5v341.3c0,15.7-12.8,28.5-28.5,28.5H56.8   c-15.7,0-28.5-12.8-28.5-28.5V120.8c0-15.7,12.8-28.5,28.5-28.5h28.5v7.2C85.3,119.1,101.3,135,120.8,135z" fill="#8995B2" jimofill=" " />\
                    	</g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Hotspot_2" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_2"   datasizewidth="223.0px" datasizeheight="50.0px" dataX="1.0" dataY="153.0"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Hotspot_3" class="imagemap firer ie-background commentable non-processed" customid="Hotspot_3"   datasizewidth="223.0px" datasizeheight="50.0px" dataX="1.0" dataY="222.0"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Hotspot_1" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_1"   datasizewidth="223.0px" datasizeheight="50.0px" dataX="1.0" dataY="87.0"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Rectangle_21" class="rectangle manualfit firer click commentable non-processed" customid="Rectangle_21"   datasizewidth="205.0px" datasizeheight="43.0px" datasizewidthpx="205.0" datasizeheightpx="42.99999999999997" dataX="1150.0" dataY="61.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_21_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_107" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_107"   datasizewidth="182.0px" datasizeheight="32.0px" dataX="1167.0" dataY="74.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_107_0">Descargar reporte en PDF</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_108" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_108"   datasizewidth="291.0px" datasizeheight="29.0px" dataX="391.0" dataY="455.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_108_0">Selecciona un color y mira tus gastos</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Hotspot_4" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_4"   datasizewidth="171.0px" datasizeheight="39.0px" dataX="633.0" dataY="512.0"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Hotspot_5" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_5"   datasizewidth="171.0px" datasizeheight="39.0px" dataX="633.0" dataY="558.0"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Hotspot_6" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_6"   datasizewidth="171.0px" datasizeheight="39.0px" dataX="633.0" dataY="604.5"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Hotspot_7" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_7"   datasizewidth="171.0px" datasizeheight="40.0px" dataX="633.0" dataY="653.0"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Hotspot_8" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_8"   datasizewidth="171.0px" datasizeheight="39.0px" dataX="633.0" dataY="700.0"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Hotspot_9" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_9"   datasizewidth="170.0px" datasizeheight="43.0px" dataX="633.5" dataY="745.5"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Hotspot_10" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_10"   datasizewidth="171.0px" datasizeheight="39.0px" dataX="633.0" dataY="796.0"  >\
                  <div class="clickableSpot"></div>\
                </div>\
                <div id="s-Rectangle_2" class="rectangle manualfit firer click ie-background commentable non-processed" customid="Rectangle 22"   datasizewidth="217.0px" datasizeheight="49.0px" datasizewidthpx="217.0" datasizeheightpx="49.0" dataX="6.0" dataY="224.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_2_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Rectangle_10" class="rectangle manualfit firer click ie-background commentable non-processed" customid="Rectangle 10"   datasizewidth="128.0px" datasizeheight="37.0px" datasizewidthpx="128.00000000000006" datasizeheightpx="37.0" dataX="14.0" dataY="82.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_10_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_10" class="group firer ie-background commentable non-processed" customid="Group 10" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Button_6" class="button multiline manualfit firer mouseenter mouseleave click commentable non-processed" customid="Secondary button"   datasizewidth="104.0px" datasizeheight="38.0px" dataX="390.0" dataY="450.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_6_0">Guardar</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_5" class="group firer ie-background commentable non-processed" customid="Group 5" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_6" class="text firer commentable non-processed" customid="Input search"  datasizewidth="163.8px" datasizeheight="26.0px" dataX="464.0" dataY="273.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Polygon_5" class="path firer commentable non-processed" customid="Chevron down icon"   datasizewidth="19.9px" datasizeheight="7.8px" dataX="603.2" dataY="282.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.92841148376465" height="7.8372883796691895" viewBox="603.2058165548096 282.0813558856288 19.92841148376465 7.8372883796691895" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Polygon_5-da5fb" d="M621.6142636469855 282.0813558856288 L613.2544643089902 287.84406765688726 L604.5568953078906 282.0813558856288 L603.2058165548096 283.00338978276955 L613.3389070519218 289.91864411437035 L623.1342281879191 283.11864397697656 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Polygon_5-da5fb" fill="#CCCCCC" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_6" class="group firer ie-background commentable non-processed" customid="Group 5" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_7" class="text firer commentable non-processed" customid="Input search"  datasizewidth="163.7px" datasizeheight="26.0px" dataX="464.0" dataY="305.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Path_5" class="path firer commentable non-processed" customid="Chevron down icon"   datasizewidth="19.9px" datasizeheight="7.8px" dataX="603.2" dataY="314.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.92841148376465" height="7.8372883796691895" viewBox="603.2058165548096 314.081355885629 19.92841148376465 7.8372883796691895" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_5-da5fb" d="M621.6142636469855 314.081355885629 L613.2544643089902 319.84406765688743 L604.5568953078906 314.081355885629 L603.2058165548096 315.0033897827697 L613.3389070519218 321.91864411437047 L623.1342281879191 315.1186439769768 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_5-da5fb" fill="#CCCCCC" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_7" class="group firer ie-background commentable non-processed" customid="Group 5" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_8" class="text firer commentable non-processed" customid="Input search"  datasizewidth="163.7px" datasizeheight="26.0px" dataX="464.0" dataY="338.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Path_6" class="path firer commentable non-processed" customid="Chevron down icon"   datasizewidth="19.9px" datasizeheight="7.8px" dataX="603.2" dataY="347.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.92841148376465" height="7.8372883796691895" viewBox="603.2058165548096 347.0813558856288 19.92841148376465 7.8372883796691895" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_6-da5fb" d="M621.6142636469855 347.0813558856288 L613.2544643089902 352.84406765688726 L604.5568953078906 347.0813558856288 L603.2058165548096 348.00338978276955 L613.3389070519218 354.91864411437035 L623.1342281879191 348.11864397697656 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_6-da5fb" fill="#CCCCCC" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_8" class="group firer ie-background commentable non-processed" customid="Group 5" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_9" class="text firer commentable non-processed" customid="Input search"  datasizewidth="163.7px" datasizeheight="26.0px" dataX="464.0" dataY="370.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Path_7" class="path firer commentable non-processed" customid="Chevron down icon"   datasizewidth="19.9px" datasizeheight="7.8px" dataX="603.2" dataY="379.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.92841148376465" height="7.8372883796691895" viewBox="603.2058165548096 379.0813558856287 19.92841148376465 7.8372883796691895" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_7-da5fb" d="M621.6142636469855 379.0813558856287 L613.2544643089902 384.84406765688715 L604.5568953078906 379.0813558856287 L603.2058165548096 380.00338978276943 L613.3389070519218 386.91864411437024 L623.1342281879191 380.11864397697644 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_7-da5fb" fill="#CCCCCC" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_9" class="group firer ie-background commentable non-processed" customid="Group 5" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_10" class="text firer commentable non-processed" customid="Input search"  datasizewidth="163.7px" datasizeheight="26.0px" dataX="464.0" dataY="400.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Path_8" class="path firer commentable non-processed" customid="Chevron down icon"   datasizewidth="19.9px" datasizeheight="7.8px" dataX="603.2" dataY="409.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.92841148376465" height="7.8372883796691895" viewBox="603.2058165548096 409.08135588562914 19.92841148376465 7.8372883796691895" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_8-da5fb" d="M621.6142636469855 409.08135588562914 L613.2544643089902 414.8440676568876 L604.5568953078906 409.08135588562914 L603.2058165548096 410.0033897827699 L613.3389070519218 416.91864411437064 L623.1342281879191 410.1186439769769 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_8-da5fb" fill="#CCCCCC" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_3" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_108"   datasizewidth="74.0px" datasizeheight="29.0px" dataX="464.0" dataY="244.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_3_0">Categoria</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_37"   datasizewidth="237.0px" datasizeheight="29.0px" dataX="281.5" dataY="208.5" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_1_0">Registrar Gasto</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_2" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_108"   datasizewidth="74.0px" datasizeheight="29.0px" dataX="281.5" dataY="244.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_2_0">Valor</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Input_1" class="text firer commentable non-processed" customid="Input search"  datasizewidth="130.5px" datasizeheight="26.0px" dataX="281.5" dataY="273.0" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="$"/></div></div>  </div></div></div>\
        <div id="s-Input_2" class="text firer commentable non-processed" customid="Input search"  datasizewidth="130.5px" datasizeheight="26.0px" dataX="281.5" dataY="307.0" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="$"/></div></div>  </div></div></div>\
        <div id="s-Input_3" class="text firer commentable non-processed" customid="Input search"  datasizewidth="130.5px" datasizeheight="26.0px" dataX="281.5" dataY="339.0" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="$"/></div></div>  </div></div></div>\
        <div id="s-Input_4" class="text firer commentable non-processed" customid="Input search"  datasizewidth="130.5px" datasizeheight="26.0px" dataX="281.5" dataY="372.0" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="$"/></div></div>  </div></div></div>\
        <div id="s-Input_5" class="text firer commentable non-processed" customid="Input search"  datasizewidth="130.5px" datasizeheight="26.0px" dataX="281.5" dataY="403.0" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="$"/></div></div>  </div></div></div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;