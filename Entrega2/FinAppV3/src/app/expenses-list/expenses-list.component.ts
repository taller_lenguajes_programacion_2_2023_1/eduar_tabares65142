import { Component } from '@angular/core';
import * as listaDeGastos from 'src/assets/gastos.json';


@Component({
  selector: 'app-expenses-list',
  templateUrl: './expenses-list.component.html',
  styleUrls: ['./expenses-list.component.css']
})
export class ExpensesListComponent {

  gastos: any = listaDeGastos;
}
