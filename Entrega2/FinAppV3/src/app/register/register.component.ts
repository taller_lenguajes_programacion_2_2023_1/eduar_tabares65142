import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from 'src/app/services/register/register.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
   firstname: string;
   lastname: string;
   phone: string;
   username: string;
   password: string;

   forma!:FormGroup;

   constructor(private registerService: RegisterService, private fb:FormBuilder) { 
    this.crearFormulario();
  }

  get nombreNoValido(){
    
    return this.forma.get('firstname1')?.invalid && this.forma.get('firstname1')?.touched;
  
  }

  get apellidoNoValido(){
    
    return this.forma.get('lastname1')?.invalid && this.forma.get('lastname1')?.touched;
  
  }

  get usuarioNoValido(){
    
    return this.forma.get('username1')?.invalid && this.forma.get('username1')?.touched;
  
  }

  get claveNoValido(){
    
    return this.forma.get('password1')?.invalid && this.forma.get('password1')?.touched;
  
  }

  crearFormulario(){
    const regUserName = /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_\-+={}[\]|\:;"'<>,.?\/])(?!.*\s).{8,}$/;
    this.forma = this.fb.group({
      firstname1:['',[Validators.required]],
      lastname1:['',[Validators.required, ]],
      phone1:['',[Validators.required, ]],
      username1:['',[Validators.required, ]],
      password1:['',[Validators.required,Validators.pattern(regUserName)]]
    });
  }

  guardar(){
    console.log(this.forma);
  }

  guardarDatos(){
    const userData = {firstName: this.firstname, lastName: this.lastname, phone: this.phone, userName: this.username, password: this.password}
    this.registerService.save('userData',userData);
    alert('Datos guardados correctamente')
    console.log(this.forma);
    this.forma.reset();
  }

}
