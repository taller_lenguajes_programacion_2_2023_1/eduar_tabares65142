import { Component } from '@angular/core';
import { Router } from '@angular/router';  
import { AuthService } from 'src/app/services/login/auth.service' 
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  username: string='';
  password: string='';
  
  user = {
    username: this.username,
    password: this.password
  }

  constructor(private authService: AuthService,private router: Router) {}

  onSubmit() {
    debugger    
      
      
      this.authService.login(this.username, this.password)
        .subscribe(result => {
          if (result) {
          
            this.router.navigate(["/ExpensesList"]);
          } else {
            if (this.authService.loginStorage(this.username,this.password)) {
              this.router.navigate(['/ExpensesList']);
            } else {
              alert('Credenciales incorrectas');
            }
          }
        });
    }

}
