package com.finapp.Finapp.domain.Gastos;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GastosCategory {

    private final String value;

    public GastosCategory(String value) {
        this.value = value;
    }
}
