package com.finapp.Finapp.domain.Ingresos;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class IngresosDetail {
    private final String value;


    public IngresosDetail(String value) {
        this.value = value;
    }
}
