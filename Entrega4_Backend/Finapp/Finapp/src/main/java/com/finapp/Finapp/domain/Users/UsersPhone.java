package com.finapp.Finapp.domain.Users;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UsersPhone {

    private final String value;

    public UsersPhone(String value) {
        this.value = value;
    }
}
