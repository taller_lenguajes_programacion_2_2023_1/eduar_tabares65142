package com.finapp.Finapp.domain.Users;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Users {

    private UsersId id;
    private UsersFirstName firstName;
    private UsersLastName lastName;
    private UsersPhone phone;
    private UsersUserName userName;
    private UsersPassword password;

}
