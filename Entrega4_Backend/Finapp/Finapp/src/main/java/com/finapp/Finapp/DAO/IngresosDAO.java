package com.finapp.Finapp.DAO;

import com.finapp.Finapp.domain.Gastos.*;
import com.finapp.Finapp.domain.Ingresos.*;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "Ingresos")
@Data
public class IngresosDAO {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_ingresos;

    @Column
    private String detail;

    @Column String category;

    @Column
    private Double value_monetary;

    public Ingresos toDomain(){
        IngresosId ingresosId = new IngresosId(this.id_ingresos);
        IngresosCategory ingresosCategory = new IngresosCategory(this.category);
        IngresosDetail ingresosDetail = new IngresosDetail(this.detail);
        IngresosValue ingresosValue = new IngresosValue(this.value_monetary);

        Ingresos ingresos = new Ingresos(ingresosId,ingresosDetail,ingresosCategory,ingresosValue);
        return ingresos;
    }

    public static IngresosDAO fromDomain(Ingresos ingresos){
        IngresosDAO ingresosDAO = new IngresosDAO();
        ingresosDAO.setValue_monetary(ingresos.getValue().getValue());
        ingresosDAO.setCategory(ingresos.getCategory().getValue());
        ingresosDAO.setDetail(ingresos.getDetail().getValue());
        ingresosDAO.setId_ingresos(ingresos.getId().getValue());
        //Long id = gastos.getId() == null ? 0 : gastos.getId().getValue();
        //gastosDAO.setId_gastos(id);
        return ingresosDAO;
    }
}
