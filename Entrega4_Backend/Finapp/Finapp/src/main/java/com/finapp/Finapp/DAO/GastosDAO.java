package com.finapp.Finapp.DAO;

import com.finapp.Finapp.domain.Gastos.*;
import lombok.Data;

import javax.persistence.*;


@Entity
@Table(name = "Gastos")
@Data
public class GastosDAO {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_gastos;

    @Column
    private String detail;

    @Column
    private String category;

    @Column
    private Double value_monetary;


    public Gastos toDomain(){
        GastosId gastosId = new GastosId(this.id_gastos);
        GastosCategory gastosCategory = new GastosCategory(this.category);
        GastosDetail gastosDetail = new GastosDetail(this.detail);
        GastosValue gastosValue = new GastosValue(this.value_monetary);

        Gastos gastos = new Gastos(gastosId, gastosDetail, gastosCategory, gastosValue);
        return gastos;
    }

    public static GastosDAO fromDomain(Gastos gastos){
        GastosDAO gastosDAO = new GastosDAO();
        gastosDAO.setValue_monetary(gastos.getValue().getValue());
        gastosDAO.setCategory(gastos.getCategory().getValue());
        gastosDAO.setDetail(gastos.getDetail().getValue());
        gastosDAO.setId_gastos(gastos.getId().getValue());
        //Long id = gastos.getId() == null ? 0 : gastos.getId().getValue();
        //gastosDAO.setId_gastos(id);
        return gastosDAO;
    }
}
