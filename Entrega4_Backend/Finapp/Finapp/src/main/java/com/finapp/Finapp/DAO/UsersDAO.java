package com.finapp.Finapp.DAO;

import com.finapp.Finapp.domain.Users.*;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "Users")
@Data
public class UsersDAO {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_users;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String phone;

    @Column
    private String userName;

    @Column
    private String password;

    public Users toDomain(){
        UsersId usersId = new UsersId(this.id_users);
        UsersFirstName usersFirstName = new UsersFirstName(this.firstName);
        UsersLastName usersLastName = new UsersLastName(this.lastName);
        UsersPhone usersPhone = new UsersPhone(this.phone);
        UsersUserName usersUserName = new UsersUserName(this.userName);
        UsersPassword usersPassword = new UsersPassword(this.password);

        Users users  = new Users(usersId, usersFirstName, usersLastName, usersPhone, usersUserName,usersPassword);
        return users;
    }

    public static UsersDAO fromDomain(Users users){
        UsersDAO usersDAO = new UsersDAO();
        usersDAO.setId_users(users.getId().getValue());
        usersDAO.setFirstName(users.getFirstName().getValue());
        usersDAO.setLastName(users.getLastName().getValue());
        usersDAO.setPhone(users.getPhone().getValue());
        usersDAO.setUserName(users.getUserName().getValue());
        usersDAO.setPassword(users.getPassword().getValue());
        //Long id = gastos.getId() == null ? 0 : gastos.getId().getValue();
        //gastosDAO.setId_gastos(id);
        return usersDAO;
    }

}
