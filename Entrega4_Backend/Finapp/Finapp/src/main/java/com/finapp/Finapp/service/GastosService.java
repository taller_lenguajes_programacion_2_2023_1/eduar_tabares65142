package com.finapp.Finapp.service;

import com.finapp.Finapp.DAO.GastosDAO;
import com.finapp.Finapp.DTO.GastosDTO;
import com.finapp.Finapp.domain.Gastos.*;
import com.finapp.Finapp.repository.GastosRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GastosService {
    private final GastosRepository gastosRepository;
    public GastosService(GastosRepository gastosRepository) {
        this.gastosRepository = gastosRepository;
    }

    public GastosDTO save(GastosDTO gastosDTO){
        //Gastos gastos = gastosDTO.toDomain();
        GastosDAO gastosDb = this.gastosRepository.findByCategory(gastosDTO.getCategory());

        if (gastosDb != null) throw new IllegalArgumentException("Name of product is not unique");

        GastosDetail gastosDetail = new GastosDetail(gastosDTO.getDetail());
        GastosCategory gastosCategory = new GastosCategory(gastosDTO.getCategory());
        GastosValue gastosValue = new GastosValue(gastosDTO.getValor());

        Gastos gastos = new Gastos(null, gastosDetail, gastosCategory, gastosValue);

        GastosDAO gastosForSave = GastosDAO.fromDomain(gastosDTO.toDomain());
        GastosDAO gastosSaved = this.gastosRepository.save(gastosForSave);
        Gastos gastosSavedWithDomain = gastosSaved.toDomain();

        return GastosDTO.fromDomain(gastosSavedWithDomain);
    }

    public List<GastosDTO> getAllGastos() {
        List<GastosDAO> gastosDAOS = this.gastosRepository.findAll();
        List<Gastos> gastos = gastosDAOS.stream().map(gastosDAO -> gastosDAO.toDomain()).collect(Collectors.toList());
        List<GastosDTO> gastosDTOS = gastos.stream().map(gasto -> GastosDTO.fromDomain(gasto)).collect(Collectors.toList());
        return gastosDTOS;
    }
}


