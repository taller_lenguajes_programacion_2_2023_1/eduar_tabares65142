package com.finapp.Finapp.domain.Gastos;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GastosId {

    private final Long value;


    public GastosId(Long value) {
        this.value = value;
    }
}
