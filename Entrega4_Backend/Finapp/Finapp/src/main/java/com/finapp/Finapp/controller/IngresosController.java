package com.finapp.Finapp.controller;

import com.finapp.Finapp.DTO.GastosDTO;
import com.finapp.Finapp.DTO.IngresosDTO;
import com.finapp.Finapp.domain.Ingresos.Ingresos;
import com.finapp.Finapp.service.IngresosService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class IngresosController {

    private final IngresosService ingresosService;

    public IngresosController(IngresosService ingresosService) {
        this.ingresosService = ingresosService;
    }

    @RequestMapping(value = "/ingresos", method = RequestMethod.POST)
    public ResponseEntity<?> saveIngreso(@RequestBody IngresosDTO ingresosDTO){
        try{
            IngresosDTO ingresosResponse = this.ingresosService.save(ingresosDTO);
            return ResponseEntity.ok(ingresosResponse);
        }catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try Later");
        }
    }

    @RequestMapping(value = "/ingresos", method = RequestMethod.GET)
    public ResponseEntity<?> getAllIngresos(){
        try {
            List<IngresosDTO> productResponse = this.ingresosService.getAllIngresos();
            return ResponseEntity.ok(productResponse);
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

}
