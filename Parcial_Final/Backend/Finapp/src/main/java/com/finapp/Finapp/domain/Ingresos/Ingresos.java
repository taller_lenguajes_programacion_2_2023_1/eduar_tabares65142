package com.finapp.Finapp.domain.Ingresos;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Ingresos {
    private IngresosId id;
    private IngresosDetail detail;
    private IngresosCategory category;
    private IngresosValue value;

}
