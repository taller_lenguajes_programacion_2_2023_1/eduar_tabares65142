package com.finapp.Finapp.DTO;

import com.finapp.Finapp.domain.Gastos.*;
import lombok.Data;

@Data
public class GastosDTO {

    private Long id;
    private String detail;
    private String category;
    private Double valor;

    public Gastos toDomain(){
        GastosId gastosId = new GastosId(id);
        GastosDetail gastosDetail = new GastosDetail(detail);
        GastosCategory gastosCategory = new GastosCategory(category);
        GastosValue gastosValue = new GastosValue(valor);

        Gastos gastos = new Gastos(gastosId,gastosDetail,gastosCategory,gastosValue);
        return gastos;
    }

    public static GastosDTO fromDomain(Gastos gastos){
        GastosDTO gastosDTO = new GastosDTO();
        gastosDTO.setId(gastos.getId().getValue());
        gastosDTO.setDetail(gastos.getDetail().getValue());
        gastosDTO.setCategory(gastos.getCategory().getValue());
        gastosDTO.setValor(gastos.getValue().getValue());
        return gastosDTO;
    }

}
