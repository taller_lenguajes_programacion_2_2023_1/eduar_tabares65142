package com.finapp.Finapp.domain.Users;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UsersId {

    private final Long value;

    public UsersId(Long value) {
        this.value = value;
    }
}
