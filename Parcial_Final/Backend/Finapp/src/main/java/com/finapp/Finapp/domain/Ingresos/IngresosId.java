package com.finapp.Finapp.domain.Ingresos;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class IngresosId {

    private final Long value;

    public IngresosId(Long value) {
        this.value = value;
    }
}
