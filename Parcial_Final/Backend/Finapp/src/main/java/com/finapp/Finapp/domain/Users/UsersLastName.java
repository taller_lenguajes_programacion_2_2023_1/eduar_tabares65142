package com.finapp.Finapp.domain.Users;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UsersLastName {

    private final String value;

    public UsersLastName(String value) {
        this.value = value;
    }
}
