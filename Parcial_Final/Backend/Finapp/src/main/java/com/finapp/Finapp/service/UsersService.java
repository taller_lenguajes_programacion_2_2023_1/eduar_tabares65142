package com.finapp.Finapp.service;

import com.finapp.Finapp.DAO.UsersDAO;
import com.finapp.Finapp.DTO.UsersDTO;
import com.finapp.Finapp.domain.Users.*;
import com.finapp.Finapp.repository.UsersRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersService {

    private final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public UsersDTO save(UsersDTO usersDTO){
        UsersDAO usersDb = this.usersRepository.findByUserName(usersDTO.getUserName());

        if (usersDb != null) throw new IllegalArgumentException("Name of product is not unique");

        UsersFirstName firstName = new UsersFirstName(usersDTO.getFirstName());
        UsersLastName lastName = new UsersLastName(usersDTO.getLastName());
        UsersPhone phone = new UsersPhone(usersDTO.getPhone());
        UsersUserName userName = new UsersUserName(usersDTO.getUserName());
        UsersPassword password = new UsersPassword(usersDTO.getPassword());

        Users users = new Users(null, firstName, lastName, phone, userName, password);

        UsersDAO usersForSave = UsersDAO.fromDomain(usersDTO.toDomain());
        UsersDAO usersSaved = this.usersRepository.save(usersForSave);
        Users userSavedWithDomain = usersSaved.toDomain();

        return UsersDTO.fromDomain(userSavedWithDomain);
    }

    public List<UsersDTO> getAllUsers() {
        List<UsersDAO> usersDAOS = this.usersRepository.findAll();
        List<Users> users = usersDAOS.stream().map(usersDAO -> usersDAO.toDomain()).collect(Collectors.toList());
        List<UsersDTO> usersDTOS = users.stream().map(user -> UsersDTO.fromDomain(user)).collect(Collectors.toList());
        return usersDTOS;
    }
}
