package com.finapp.Finapp.domain.Gastos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Gastos {
    private GastosId id;
    private GastosDetail detail;
    private GastosCategory category;
    private GastosValue value;

}
