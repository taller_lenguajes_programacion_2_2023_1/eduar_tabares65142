package com.finapp.Finapp.domain.Gastos;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GastosValue {

    private final Double value;

    public GastosValue(Double value) {
        this.value = value;
    }
}
