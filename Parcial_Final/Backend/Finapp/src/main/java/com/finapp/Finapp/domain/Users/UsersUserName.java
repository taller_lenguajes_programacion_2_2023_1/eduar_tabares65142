package com.finapp.Finapp.domain.Users;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UsersUserName {
    private final String value;

    public UsersUserName(String value) {
        this.value = value;
    }
}
