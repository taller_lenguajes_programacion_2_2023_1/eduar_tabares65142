package com.finapp.Finapp.domain.Gastos;

import lombok.*;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class GastosDetail {
    private final String value;

    public GastosDetail(String value) {
        Validate.notNull(value, "El Detalle no puede ser nulo");
        this.value = value;
    }
}
