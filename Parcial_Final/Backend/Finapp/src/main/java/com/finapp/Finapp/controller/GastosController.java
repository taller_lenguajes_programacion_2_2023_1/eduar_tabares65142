package com.finapp.Finapp.controller;

import com.finapp.Finapp.DAO.GastosDAO;
import com.finapp.Finapp.DTO.GastosDTO;
import com.finapp.Finapp.domain.Gastos.Gastos;
import com.finapp.Finapp.service.GastosService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@RestController
public class GastosController {
    private final GastosService gastosService;

    public GastosController(GastosService gastosService) {
        this.gastosService = gastosService;
    }

    @RequestMapping(value = "/gastos", method = RequestMethod.POST)
    public ResponseEntity<?> saveGasto(@RequestBody GastosDTO gastosDTO){
        try{
            GastosDTO gastosResponse = this.gastosService.save(gastosDTO);
            return ResponseEntity.ok(gastosResponse);
        }catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try Later");
        }
    }

    @RequestMapping(value = "/gastos", method = RequestMethod.GET)
    public ResponseEntity<?> getAllGastos(){
        try {
            List<GastosDTO> productResponse = this.gastosService.getAllGastos();
            return ResponseEntity.ok(productResponse);
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }
}


